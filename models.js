const mongoose = require('mongoose');
const {  bookSchema } = require('./schemas');

const bookModel = mongoose.model('Book', bookSchema);

module.exports = {
  bookModel
};